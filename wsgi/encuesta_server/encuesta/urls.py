# coding:utf8
from django.conf.urls import patterns, include, url
from encuesta import views

urlpatterns = [
    url(r'^api/sucursales$', views.sucursalJSON, name='sucursalJSON'),

    url(r'^api/sucursales/(?P<sucursal_id>\d+)$', views.sucursalDetailJSON, name='sucursalDetailJSON'),

    url(r'^api/encuestas$', views.encuestaJSON, name='encuestaJSON'),

    url(r'^api/encuestas/(?P<encuesta_id>\d+)$', views.detailEncuestaJSON, name='detailEncuestaJSON'),

    url(r'^api/encuestas/vote$', views.voteEncuestaJSON, name='voteEncuestaJSON'),

    url(r'^api/respuestas$', views.respuestaJSON, name='respuestaJSON'),

    url(r'^api/sugerencias$', views.sugerenciaJSON, name='sugerenciaJSON'),

    url(r'^api/sugerencias/fecha$', views.sugerenciaFechaJSON, name='sugerenciaFechaJSON'),

    url(r'^api/resultados/funcionarios$', views.funcionariosJSON, name='funcionariosJSON'),

    url(r'^api/funcionarios/(?P<encuesta_id>\d+)$', views.listafuncionariosJSON, name='listafuncionariosJSON'),

    url(r'^api/respuesta/$', views.respuestaDetailJSON, name='respuestaDetailJSON'),

    url(r'^api/questions$', views.indexJSON, name='indexJSON'),

    url(r'^api/resultados$', views.detailJSON, name='detailJSON'),

    url(r'^api/resultados/(?P<encuesta_id>\d+)$', views.detallePreguntasJSON, name='detallePreguntasJSON'),
]
