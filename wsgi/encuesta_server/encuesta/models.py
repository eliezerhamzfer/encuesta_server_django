# coding:utf-8
# import from django database, all models
from django.db import models
from django.conf import settings
# from PIL import Image
from django.utils.text import slugify
from django.utils.safestring import mark_safe
import os
from datetime import date
import json
from django.core.serializers import serialize
from django.core import serializers

# Create your models here.

class Sucursal(models.Model):

    name = models.CharField(max_length=60, verbose_name='Sucursal')

    direccion = models.CharField(max_length=140, verbose_name='Dirección')

    slug_sucursal = models.SlugField(max_length=50, blank=True, verbose_name='Slug de la sucursal')

    def __str__(self):
        return self.name.encode('utf-8')

    def save(self):
        self.slug_sucursal = slugify(self.name)
        super(Sucursal, self).save()

    def toDict(self):
        output = {
                'id_sucursal': self.id,
                'name': self.name,
                'direccion': self.direccion,
                'slug': self.slug_sucursal,
            }
        return output

    def toJSON(self):
        toJson = json.dumps(self.toDict(),
        encoding = 'utf-8',
        sort_keys = False,
        ensure_ascii = False,
        indent = 4,
        separators = (', ',': ')).encode('utf8')
        return toJson


class Funcionario(models.Model):

    name = models.CharField(max_length=60, verbose_name='Nombre')

    sucursal = models.ForeignKey(Sucursal, verbose_name='Lugar de trabajo')

    def __str__(self):
        return self.name.encode('utf-8')

    def toDict(self):
        output = {
            'id_funcionario': self.pk,
            'name': self.name,
        }
        return output

    def toJSON(self):
        toJson = json.dumps(self.toDict(),
        encoding = 'utf8',
        sort_keys = False,
        ensure_ascii = False,
        indent = 4,
        separators = (', ',': ')).encode('utf8')
        return toJson


class Encuesta(models.Model):

    nombre = models.CharField(max_length=300, verbose_name='Encuesta')

    descripcion = models.TextField()

    ruta_local = models.CharField(max_length=300, default='http://',verbose_name='ruta local')

    sucursal = models.ForeignKey(Sucursal, verbose_name='Sucursal de la encuesta')

    slug_encuesta = models.SlugField(max_length=50, blank=True, verbose_name='Slug de la encuesta')

    def __str__(self):
        return self.nombre.encode('utf-8')

    def save(self):
        self.slug_encuesta = slugify(self.nombre)
        super(Encuesta, self).save()

    def toDict(self):
        questionList = []
        for q in self.question_set.all():
            choiceList = []
            for c in q.choice_set.all():
                choiceList.append({
                    u'id_opcion': c.pk,
                    u'value': c.choice_text,
                    u'input_type': c.tipo,
                    u'votes': c.votes,
                    u'imagen': c.icon,
                })
            questionList.append({
                'id_pregunta': q.pk,
                'text': q.question_text,
                'answers': choiceList,
            })

        output = {
                'id_encuesta': self.id,
                'ip_route': self.ruta_local,
                'name': self.nombre,
                'slug': self.slug_encuesta,
                'questions': questionList,
            }
        return output

    def toJSON(self):
        toJson = json.dumps(self.toDict(),
        encoding = 'utf8',
        sort_keys = False,
        ensure_ascii = False,
        indent = 4,
        separators = (', ',': ')).encode('utf8')
        return toJson

    def toDictDetail(self):
        questionLista = []
        for q in self.question_set.all():
            choiceList = []
            for c in q.choice_set.all():
                choiceList.append({
                    u'id_opcion': c.pk,
                    u'value': c.choice_text,
                    u'votes': c.votes,
                })
            questionLista.append({
                'id_pregunta': q.pk,
                'text': q.question_text,
                'answers': choiceList,
            })

        out = {
                'id_encuesta': self.id,
                'name': self.nombre,
                'slug': self.slug_encuesta,
                'questions': questionLista,
            }
        return out

    def resToJSON(self):
        resToJSON = json.dumps(self.toDictDetail(),
        encoding = 'utf8',
        sort_keys = False,
        ensure_ascii = False,
        indent = 4,
        separators = (', ',': ')).encode('utf8')
        return resToJSON


class Categoria(models.Model):

    name = models.CharField(max_length=200)

    categoria_encuesta = models.ForeignKey(Encuesta)

    def __unicode__(self):
        return self.name.encode('utf-8')


class Question(models.Model):
    question_text = models.CharField(max_length=200, verbose_name='Pregunta')
    categoria = models.ForeignKey(Categoria, blank=True, null=True)
    encuesta = models.ForeignKey(Encuesta)
    pub_date = models.DateTimeField('Fecha de publicación')

    def __str__(self):
        return self.question_text.encode('utf-8')

    def toDict(self):
        votesList = []
        for c in self.choice_set.all():
            votesList.append({
                'id': c.pk,
                'value': c.choice_text,
                'input_type': c.tipo,
                'votes': c.votes,
                'avatar': c.icon,
            })
        output = {
                'id': self.id,
                'text': self.question_text,
                'answers': votesList,
            }
        return output

    def toJSON(self):
        toJson = json.dumps(self.toDict(),
        encoding = 'utf-8',
        sort_keys = False,
        ensure_ascii = False,
        indent = 4,
        separators = (', ',': ')).encode('utf8')
        return toJson


class Choice(models.Model):
    TEXT = 'text'
    TEXTAREA = 'textarea'
    RADIO = 'radio'
    CHECKBOX = 'checkbox'
    SELECT = 'select'
    SELECT_MULTIPLE = 'select-multiple'
    INTEGER = 'integer'
    EMAIL = 'email'
    NUMBER = 'number'

    INPUT_TYPES = (
        ("", 'Selecciona tipo'),
        (TEXT, 'text'),
        (TEXTAREA, 'textarea'),
        (RADIO, 'radio'),
        (CHECKBOX, 'checkbox'),
        (SELECT_MULTIPLE, 'Select Multiple'),
        (INTEGER, 'integer'),
        (EMAIL, 'email'),
        (NUMBER, 'number'),
    )

    AVATAR = 'images/emoticons/avatar.png'

    AVATARS = (
        ("", 'Selecciona Icono'),
        ('images/funcionarios/avatar.png', 'Cervena - Suc 3'),
        ('images/funcionarios/avatar.png', 'Charlotte - Suc 3'),
        ('images/funcionarios/avatar.png', 'Liam - Suc 3'),
        ('images/funcionarios/avatar.png', 'Rose Marie - Suc 2'),
        ('images/funcionarios/avatar.png', 'Lenna Katina - Suc 2'),
        ('images/funcionarios/avatar.png', 'John Doe - Suc 2'),
        ('images/funcionarios/avatar.png', 'Shmi Skywalker  - Suc 1'),
        ('images/funcionarios/avatar.png', 'Anakin Skywalker  - Suc 1'),
        ('images/funcionarios/avatar.png', 'Luke Skywalker - Suc 1'),
        ('images/emoticons/avatar.png', 'User Avatar'),
        ('images/emoticons/bad.png', 'Mala'),
        ('images/emoticons/sad.png', 'Regular'),
        ('images/emoticons/buena.png', 'Buena'),
        ('images/emoticons/happy.png', 'Agradable'),
        ('images/emoticons/love.png', 'Excelente'),
    )

    PREGUNTA = 'id_pregunta'
    OPCION = 'textarea'
    RADIO = 'radio'
    CHECKBOX = 'checkbox'
    SELECT = 'select'
    SELECT_MULTIPLE = 'select-multiple'
    INTEGER = 'integer'
    EMAIL = 'email'
    NUMBER = 'number'

    INPUT_TYPES = (
        ("", 'Selecciona tipo'),
        (TEXT, 'text'),
        (TEXTAREA, 'textarea'),
        (RADIO, 'radio'),
        (CHECKBOX, 'checkbox'),
        (SELECT_MULTIPLE, 'Select Multiple'),
        (INTEGER, 'integer'),
        (EMAIL, 'email'),
        (NUMBER, 'number'),
    )

    question = models.ForeignKey(Question)

    choice_text = models.CharField(max_length=200, verbose_name='Opcion')

    icon = models.CharField(max_length=140, choices=AVATARS, default='Selecciona Icono', blank=True)

    tipo = models.CharField(max_length=50, choices=INPUT_TYPES, default=RADIO, verbose_name='Input')

    # encuesta = models.ForeignKey(Encuesta, blank=True)

    votes = models.IntegerField(default=0, verbose_name='Veces votadas')

    slug = models.SlugField(max_length=50, blank=True, verbose_name='Slug de la opcion')

    def save(self):

        self.slug = slugify(self.choice_text)

        super(Choice, self).save()

    def __str__(self):
        return self.choice_text.encode('utf-8')
        # para cadenas de texto con caracteres especiales


class Answer(models.Model):

    encuesta = models.ForeignKey(Encuesta)

    pregunta = models.ForeignKey(Question)

    opcion = models.ForeignKey(Choice, blank=True, null=True)

    votes = models.IntegerField(default=0, verbose_name='Veces votadas en esta fecha')

    created = models.DateTimeField('Fecha y hora de creación')

    uuid = models.CharField(max_length=200, verbose_name='Identificador', default='identificar')

    def __str__(self):
        return self.pregunta.question_text.encode('utf-8')

    def toDict(self):
        preguntaRespondidasList = []
        preguntaRespondidasList.append({
            'id_respuesta': self.pk,
            'encuesta': self.encuesta,
            'pregunta': self.pregunta,
            'opcion': self.opcion,
            'fecha': self.created.strftime('%H:%M:%S %d-%m-%Y'),
        })
        return preguntaRespondidasList

    def toJSON(self):
        toJson = json.dumps(self.toDict()).encode('utf8')
        return toJson


class AnswerCAB(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    created = models.DateTimeField('Fecha de creación')
    sugerencia = models.TextField('Sugerencia adicional', blank=True, default="Sin sugerencia")
    uuid = models.CharField(max_length=200, verbose_name='Identificador', default='identificar')

    def __str__(self):
        return self.sugerencia.encode('utf-8')
