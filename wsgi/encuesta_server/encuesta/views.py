# coding:utf-8
from django.shortcuts import get_list_or_404, get_object_or_404
from django.http import HttpResponse
from encuesta.models import *
from django.views.decorators.csrf import csrf_exempt
from datetime import date
from datetime import datetime
import datetime as new_datetime
import json
from django.http import JsonResponse
from django.core import serializers
from StringIO import StringIO
from django.db.models import Count, Min, Sum, Avg
from collections import Counter
import uuid

# REST api
def indexJSON(request):
    latest_question_list = list(Question.objects.order_by('encuesta'))
    # la funcion map recorre una lista de elementos
    lista = map(lambda x: x.toDict(), latest_question_list)
    response = HttpResponse(json.dumps(lista,
        encoding = 'utf-8',
        ensure_ascii = False,
        sort_keys = False,
        indent = 4,
        separators = (', ',': ')
    ).encode('utf8'), content_type='application/json')
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Methods'] = 'GET, OPTIONS'
    return response


def sucursalJSON(request):
    sucursalList = list(Sucursal.objects.all())
    lista = map(lambda x: x.toDict(), sucursalList)
    response = HttpResponse(json.dumps(lista,
        encoding = 'utf8',
        sort_keys = False,
        ensure_ascii = False,
        indent = 4,
        separators = (', ',': ')
    ).encode('utf8'), content_type='application/json')
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Methods'] = 'GET, OPTIONS'
    return response


def sucursalDetailJSON(request, sucursal_id):
    sucursal = get_object_or_404(Sucursal, pk=sucursal_id)
    response = HttpResponse(sucursal.toJSON(), content_type='application/json')
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Methods'] = 'GET, OPTIONS'
    return response


def encuestaJSON(request):
    encuestaList = list(Encuesta.objects.all())
    lista = map(lambda x: x.toDict(), encuestaList)
    response = HttpResponse(json.dumps(lista,
        encoding = 'utf-8',
        sort_keys = True,
        ensure_ascii = False,
        indent = 4,
        separators = (', ',': ')
    ).encode('utf8'), content_type='application/json')
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Methods'] = 'GET, OPTIONS'
    return response


def detailEncuestaJSON(request, encuesta_id):
    encuesta = get_object_or_404(Encuesta, pk=encuesta_id)
    response = HttpResponse(encuesta.toJSON(), content_type='application/json')
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Methods'] = 'GET, OPTIONS'
    return response


def detailJSON(request):
    responseList = list(Encuesta.objects.all())
    toLista = map(lambda y: y.toDictDetail(), responseList)
    response = HttpResponse(json.dumps(toLista,
        encoding = 'utf-8',
        sort_keys = True,
        ensure_ascii = False,
        indent = 4,
        separators = (', ',': ')
    ).encode('utf8'), content_type='application/json')
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Methods'] = 'GET, OPTIONS'
    return response

def detallePreguntasJSON(request, encuesta_id):
    encuesta = get_object_or_404(Encuesta, pk=encuesta_id)
    response = HttpResponse(encuesta.resToJSON(), content_type='application/json')
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Methods'] = 'GET, OPTIONS'
    return response


@csrf_exempt
def listafuncionariosJSON(request, encuesta_id):
    funcionario = get_list_or_404(Funcionario, sucursal=encuesta_id)
    lista = map(lambda x: x.toDict(), funcionario)
    response = HttpResponse(json.dumps(lista,
        encoding = 'utf-8',
        sort_keys = True,
        ensure_ascii = False,
        indent = 4,
        separators = (', ',': ')
    ).encode('utf8'), content_type='application/json')
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Methods'] = 'GET, OPTIONS'
    return response


@csrf_exempt
def voteEncuestaJSON(request):
    if request.method == 'OPTIONS':
        response = HttpResponse('options ok')
        response['Access-Control-Allow-Origin'] = '*'
        response['Access-Control-Allow-Methods'] = 'POST, OPTIONS'
        response['Access-Control-Allow-Headers'] = 'Content-Type'
        return response
    data = json.loads(request.body)
    id_encuesta = data['id_encuesta']
    if data['sugerencia']:
        sugerencias = data['sugerencia']
    else:
        sugerencias = str('sin sugerencia')
    answersDict = data['answers']
    try:
        e = get_object_or_404(Encuesta, pk=id_encuesta)
    except (KeyError, Encuesta.DoesNotExist):
        respuesta = {'success': False}
        response = HttpResponse(respuesta)
        response['Access-Control-Allow-Origin'] = '*'
        response['Access-Control-Allow-Methods'] = 'POST, OPTIONS'
        response['Access-Control-Allow-Headers'] = 'Content-Type'
        return response
    else:
        e = Encuesta.objects.get(pk=id_encuesta)
        ido = Choice.objects.get(pk=answersDict[1]['id_opcion'])
        ansCAB = AnswerCAB(encuesta=e, created=datetime.now(), sugerencia=sugerencias, uuid=ido)
        ansCAB.save()
        for answer in answersDict:
            e = Encuesta.objects.get(pk=id_encuesta)
            p = Question.objects.get(pk=answer['id_pregunta'])
            o = Choice.objects.get(pk=answer['id_opcion'])
            answ = Answer(encuesta=e, pregunta=p, opcion=o, created=datetime.now())
            answ.save()
            opcion = p.choice_set.get(pk=answer['id_opcion'])
            opcion.votes += 1
            opcion.save()
        respuesta = {'success': True}
        respuestaJSON = json.dumps(respuesta)
        response = HttpResponse(respuestaJSON)
        response['Access-Control-Allow-Origin'] = '*'
        response['Access-Control-Allow-Methods'] = 'POST, OPTIONS'
        response['Access-Control-Allow-Headers'] = 'Content-Type'
        return response


@csrf_exempt
def respuestaDetailJSON(request):
    if request.method == 'OPTIONS':
        response = HttpResponse('options ok')
        response['Access-Control-Allow-Origin'] = '*'
        response['Access-Control-Allow-Methods'] = 'POST, OPTIONS'
        response['Access-Control-Allow-Headers'] = 'Content-Type'
        return response

    respuesta = json.loads(request.body)
    encuesta_id = respuesta['id_encuesta']

    desdefecha = str(respuesta['desde_fecha'])
    full_desdefecha = datetime.strptime(desdefecha, '%Y-%m-%d')
    strdesdefecha = full_desdefecha.strftime('%Y-%m-%d')
    hastafecha = str(respuesta['hasta_fecha'])
    full_hastafecha = datetime.strptime(hastafecha, '%Y-%m-%d')
    new_hastafehca = full_hastafecha + new_datetime.timedelta(days=1)
    strhastafecha = new_hastafehca.strftime('%Y-%m-%d')

    responseList = Answer.objects.filter(
        encuesta=encuesta_id,
        created__range=[strdesdefecha, strhastafecha])
    data = serializers.serialize('json', responseList)
    datos = json.loads(data)
    dataLen = len(datos)
    array = []
    preguntas_contestadas = []
    opciones_seleccionadas = []
    questionList = []
    preg = Question.objects.filter(encuesta=encuesta_id)
    for i in range(0, dataLen):
        item = datos[i]
        f = item['fields']
        q = Question.objects.get(pk=f['pregunta'])
        o = Choice.objects.get(pk=f['opcion'])
        try:
            s = AnswerCAB.objects.filter(created=f['created'])
        except AnswerCAB.DoesNotExist:
            s = str('no hay sugerencia')
        array.append({
            'id_encuesta': f['encuesta'],
            'pregunta': str(q),
            'id_respuesta': f['opcion'],
            'respuesta': str(o),
            'fecha': str(f['created']),
            'sugerencia': str(s),
        })
    for value in array:
        if value['pregunta'] not in preguntas_contestadas:
            preguntas_contestadas.append(
                value['pregunta']
                )
        if value['id_respuesta'] not in preguntas_contestadas:
            opciones_seleccionadas.append(
                value['id_respuesta']
                )
    conteo_opciones = {
        i: opciones_seleccionadas.count(i) for i in opciones_seleccionadas
        }
    for q in preg:
        choiceList = []
        for c in q.choice_set.all():
            for key, value in conteo_opciones.items():
                if c.pk == key:
                    choiceList.append({
                        u'id_opcion': c.pk,
                        u'opcion': c.choice_text,
                        u'votos': value,
                    })
                elif c.pk not in conteo_opciones:
                    choiceList.append({
                        u'id_opcion': c.pk,
                        u'opcion': c.choice_text,
                        u'votos': 0,
                    })
                    break
        questionList.append({
            u'texto_pregunta': q.question_text,
            u'opciones': choiceList,
        })
    if array:
        datosParaCliente = []
        datosParaCliente.append({
                'success': True,
                'respuesta': array,
                'detalles': questionList,
            })
    else:
        datosParaCliente = []
        datosParaCliente.append({
                'success': False,
                'message': 'No existen registros',
            })
    response = HttpResponse(json.dumps(datosParaCliente,
        encoding = 'utf-8',
        sort_keys = True,
        ensure_ascii = True,
        indent = 4,
        separators = (', ',': ')
    ),content_type='application/json')
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Methods'] = 'GET, OPTIONS'
    return response


@csrf_exempt
def respuestaJSON(request):
    if request.method == 'OPTIONS':
        response = HttpResponse('options ok')
        response['Access-Control-Allow-Origin'] = '*'
        response['Access-Control-Allow-Methods'] = 'POST, OPTIONS'
        response['Access-Control-Allow-Headers'] = 'Content-Type'
        return response

    respuesta = json.loads(request.body)
    encuesta_id = respuesta['id_encuesta']
    responseList = Answer.objects.filter(
        encuesta=encuesta_id)
    data = serializers.serialize('json', responseList)
    datos = json.loads(data)
    dataLen = len(datos)
    array = []
    for i in range(0, dataLen):
        item = datos[i]
        f = item['fields']
        q = Question.objects.get(pk=f['pregunta'])
        o = Choice.objects.get(pk=f['opcion'])
        try:
            s = AnswerCAB.objects.filter(created=f['created'])
        except AnswerCAB.DoesNotExist:
            s = str('no hay sugerencia')
        array.append({
            'id_encuesta': f['encuesta'],
            'pregunta': str(q),
            'respuesta': str(o),
            'fecha': str(f['created']),
            'sugerencia': str(s)
        })
    if array:
        datosParaCliente = []
        datosParaCliente.append({
                'success': True,
                'respuesta': array,
            })
    else:
        datosParaCliente = []
        datosParaCliente.append({
                'success': False,
                'message': 'No existen registros',
            })
    response = HttpResponse(json.dumps(datosParaCliente,
        encoding = 'utf-8',
        sort_keys = True,
        ensure_ascii = False,
        indent = 4,
        separators = (', ',': ')
    ),content_type='application/json')
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Methods'] = 'GET, OPTIONS'
    return response


@csrf_exempt
def sugerenciaJSON(request):
    if request.method == 'OPTIONS':
        response = HttpResponse('options ok')
        response['Access-Control-Allow-Origin'] = '*'
        response['Access-Control-Allow-Methods'] = 'POST, OPTIONS'
        response['Access-Control-Allow-Headers'] = 'Content-Type'
        return response

    respuesta = json.loads(request.body)
    encuesta_id = respuesta['id_encuesta']
    responseList = AnswerCAB.objects.filter(
        encuesta=encuesta_id)
    data = serializers.serialize('json', responseList)
    datos = json.loads(data)
    dataLen = len(datos)
    array = []
    for i in range(0, dataLen):
        item = datos[i]
        f = item['fields']
        try:
            s = AnswerCAB.objects.get(created=f['created'])
            s2 = s.sugerencia
        except AnswerCAB.DoesNotExist:
            s = str('no hay sugerencia')
        array.append({
            'id_encuesta': f['encuesta'],
            'fecha': str(f['created']),
            'sugerencia': s2
        })
    if array:
        datosParaCliente = []
        datosParaCliente.append({
                'success': True,
                'respuesta': array,
            })
    else:
        datosParaCliente = []
        datosParaCliente.append({
                'success': False,
                'message': 'No existen registros',
            })
    response = HttpResponse(json.dumps(datosParaCliente,
        encoding = 'utf-8',
        sort_keys = True,
        ensure_ascii = False,
        indent = 4,
        separators = (', ',': ')
    ),content_type='application/json')
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Methods'] = 'GET, OPTIONS'
    return response


@csrf_exempt
def sugerenciaFechaJSON(request):
    if request.method == 'OPTIONS':
        response = HttpResponse('options ok')
        response['Access-Control-Allow-Origin'] = '*'
        response['Access-Control-Allow-Methods'] = 'POST, OPTIONS'
        response['Access-Control-Allow-Headers'] = 'Content-Type'
        return response

    respuesta = json.loads(request.body)
    encuesta_id = respuesta['id_encuesta']

    desdefecha = str(respuesta['desde_fecha'])
    full_desdefecha = datetime.strptime(desdefecha, '%Y-%m-%d')
    strdesdefecha = full_desdefecha.strftime('%Y-%m-%d')
    hastafecha = str(respuesta['hasta_fecha'])
    full_hastafecha = datetime.strptime(hastafecha, '%Y-%m-%d')
    new_hastafehca = full_hastafecha + new_datetime.timedelta(days=1)
    strhastafecha = new_hastafehca.strftime('%Y-%m-%d')

    responseList = AnswerCAB.objects.filter(
        encuesta=encuesta_id,
        created__range=[strdesdefecha, strhastafecha])
    data = serializers.serialize('json', responseList)
    datos = json.loads(data)
    dataLen = len(datos)
    array = []
    for i in range(0, dataLen):
        item = datos[i]
        f = item['fields']
        try:
            s = AnswerCAB.objects.get(created=f['created'])
            s2 = s.sugerencia
        except AnswerCAB.DoesNotExist:
            s = str('no hay sugerencia')
        array.append({
            'id_encuesta': f['encuesta'],
            'fecha': str(f['created']),
            'sugerencia': s2,
        })
    if array:
        datosParaCliente = []
        datosParaCliente.append({
                'success': True,
                'respuesta': array,
            })
    else:
        datosParaCliente = []
        datosParaCliente.append({
                'success': False,
                'message': 'No existen registros',
            })
    response = HttpResponse(json.dumps(datosParaCliente,
        encoding = 'utf-8',
        sort_keys = True,
        ensure_ascii = True,
        indent = 4,
        separators = (', ',': ')
    ),content_type='application/json')
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Methods'] = 'GET, OPTIONS'
    return response


@csrf_exempt
def funcionariosJSON(request):
    if request.method == 'OPTIONS':
        response = HttpResponse('options ok')
        response['Access-Control-Allow-Origin'] = '*'
        response['Access-Control-Allow-Methods'] = 'POST, OPTIONS'
        response['Access-Control-Allow-Headers'] = 'Content-Type'
        return response
    # respuesta es lo que recibo del usuario
    respuesta = json.loads(request.body)
    encuesta_id = respuesta['id_encuesta']
    # identifico el id de la opcion del funcionario
    usuario = respuesta['usuario']
    opcion_usuario = Choice.objects.get(choice_text=usuario)
    id_opcion_usuario = opcion_usuario.pk
    # transformo la fecha que recibo para consultar en DB

    desdefecha = str(respuesta['desde_fecha'])
    full_desdefecha = datetime.strptime(desdefecha, '%Y-%m-%d')
    strdesdefecha = full_desdefecha.strftime('%Y-%m-%d')
    hastafecha = str(respuesta['hasta_fecha'])
    full_hastafecha = datetime.strptime(hastafecha, '%Y-%m-%d')
    new_hastafehca = full_hastafecha + new_datetime.timedelta(days=1)
    strhastafecha = new_hastafehca.strftime('%Y-%m-%d')
    # busco respuestas con el rango de fecha y id de la encuesta
    responseList = Answer.objects.filter(
        encuesta=encuesta_id,
        created__range=[strdesdefecha, strhastafecha])
    # convierto a json las respuestas
    data = serializers.serialize('json', responseList)
    # convierto a una lista en python
    datos = json.loads(data)
    dataLen = len(datos)
    # en array almacenare mi lista de respuestas
    array = []
    # en preguntas contestadas almaceno las preguntas contestadas
    # sin que se repitan
    preguntas_contestadas = []
    # en opciones seleccionadas guardo la opciones sin que se repitan
    opciones_seleccionadas = []
    # en questionlist guardo pregunta y sus opciones
    questionList = []
    for i in range(0, dataLen):
        item = datos[i]
        f = item['fields']
        q = Question.objects.get(pk=f['pregunta'])
        o = Choice.objects.get(pk=f['opcion'])
        ido = o.pk
        try:
            s = AnswerCAB.objects.get(created=f['created'])
            s2 = s.uuid
        except AnswerCAB.DoesNotExist:
            s = str('no hay sugerencia')
        array.append({
            'id_encuesta': f['encuesta'],
            'id_pregunta': ido,
            'opcion_user': id_opcion_usuario,
            'pregunta': str(q),
            'id_respuesta': f['opcion'],
            'respuesta': str(o),
            'fecha': str(f['created']),
            'sugerencia': str(s),
            'uuid': str(s2)
        })

    array[:] = [d for d in array if d.get('uuid') == str(opcion_usuario)]
    # filtro las preguntas por id encuesta
    preg = Question.objects.filter(encuesta=encuesta_id)
    # aqui almaceno las preguntas sin que se repitan
    for value in array:
        if value['pregunta'] not in preguntas_contestadas:
            preguntas_contestadas.append(
                value['pregunta']
            )
        if value['id_respuesta'] not in preguntas_contestadas:
            opciones_seleccionadas.append(
                value['id_respuesta']
                )
    # aqui hago un contador de las opciones que se repiten (por su id)
    conteo_opciones = {
        i: opciones_seleccionadas.count(i) for i in opciones_seleccionadas
        }
    # aqui recorro la opcion de la pregunta y si son iguales, los almaceno
    for q in preg:
        choiceList = []
        labels_chart = []
        data_chart = []
        for c in q.choice_set.all():
            for key, value in conteo_opciones.items():
                if c.pk == key:
                    labels_chart.append(c.choice_text)
                    data_chart.append(value)
                    choiceList.append({
                        u'id_opcion': c.pk,
                        u'opcion': c.choice_text,
                        u'votos': value,
                    })
                    break
        questionList.append({
            u'texto_pregunta': q.question_text,
            u'opciones': choiceList,
            u'labels': labels_chart,
            u'data': data_chart,
        })
    for value in array:
        del value['id_encuesta']
        del value['id_pregunta']
        del value['id_respuesta']
        del value['opcion_user']
        del value['pregunta']
        del value['respuesta']
        del value['uuid']
    seen = set()
    sugerencias = []
    for value in array:
        tupla = tuple(value.items())
        if tupla not in seen:
            seen.add(tupla)
            sugerencias.append(value)
    data_to_user = []
    data_to_user.append({
        'sugerencias': sugerencias,
        'detalles': questionList,
        })
    if array:
        datosParaCliente = []
        datosParaCliente.append({
                'success': True,
                'data': data_to_user
            })
    else:
        datosParaCliente = []
        datosParaCliente.append({
                'success': False,
                'message': 'No existen registros',
            })
    response = HttpResponse(json.dumps(datosParaCliente,
        encoding = 'utf-8',
        sort_keys = True,
        ensure_ascii = True,
        indent = 4,
        separators = (', ',': ')
    ),content_type='application/json')
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Methods'] = 'GET, OPTIONS'
    return response
