# coding:utf8
from django.contrib import admin
from encuesta.models import *
# Register your models here.


admin.site.register(Sucursal)


class EncuestaAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'descripcion', 'sucursal')
    list_filter = ['sucursal']

admin.site.register(Encuesta, EncuestaAdmin)


class FuncionarioAdmin(admin.ModelAdmin):
    list_display = ('name', 'sucursal')
    list_filter = ['sucursal']

admin.site.register(Funcionario, FuncionarioAdmin)


class OptionsInline(admin.TabularInline):
    model = Choice
    extra = 0


class QuestionAdmin(admin.ModelAdmin):
    inlines = [OptionsInline]
    list_display = ('question_text', 'encuesta', 'categoria', 'pub_date',)
    list_filter = ['encuesta__sucursal', 'categoria']

admin.site.register(Question, QuestionAdmin)


class ChoiceAdmin(admin.ModelAdmin):
    list_display = ('choice_text', 'question', 'votes',)
    list_filter = ['question__encuesta']

admin.site.register(Choice, ChoiceAdmin)


class AnswerAdmin(admin.ModelAdmin):
    list_display = ('pregunta', 'encuesta', 'opcion', 'created',)
    list_filter = ['encuesta__sucursal', 'created', 'opcion']

admin.site.register(Answer, AnswerAdmin)


class AnswerCabAdmin(admin.ModelAdmin):
    list_display = ('sugerencia', 'encuesta', 'created')
    list_filter = ['encuesta__sucursal', 'created', 'uuid']

admin.site.register(AnswerCAB, AnswerCabAdmin)
admin.site.register(Categoria)
