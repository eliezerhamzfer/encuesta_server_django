"""
WSGI config for encuesta_server project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

# GETTING-STARTED: change 'encuesta_server' to your project name:
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "encuesta_server.settings")

application = get_wsgi_application()
